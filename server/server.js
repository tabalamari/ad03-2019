
const fs = require('fs');
const express = require('express');//У випадку Express, модуль експортує функцію, 
//яка може бути використана для створення екземпляра програми. Ми щойно призначили цю функцію змінній express.

//require - це ключове слово JavaScript, специфічне для Node.js, 
//і воно використовується для імпорту інших модулів. 
//Це ключове слово не є частиною JavaScript на стороні браузера, 
//оскільки не існувало концепції включення інших файлів JavaScript. 
//Всі необхідні сценарії були включені безпосередньо лише у файли HTML. 
// _________________________________________________________________________________________________
// const typeDefs = `
//   type Query {
//     about: String!
//   }
//   type Mutation {
//     setAboutMessage(message: String!): String!
//   }
// `;
// ___________________________________________________________________________________________________
const { ApolloServer } = require('apollo-server-express');
const { GraphQLScalarType } = require('graphql');
let aboutMessage = "Issue Tracker API v1.0";
const issuesDB = [
    {
        id: 1, status: 'New', owner: 'Rrrrrravan', effort: 5,
        created: new Date('2019-01-15'), due: undefined,
        title: 'Error in console when clickong Add', 
    }, {
        id: 2, status: 'Assigned', owner: 'Eddie', effort: 14,
        created: new Date('2019-01-16'), due: new Date('2019-02-01'),
        title: 'Missing bottom border on panel', 
    },
];
const GraphQLDate = new GraphQLScalarType({
    name: 'GraphQLDate',
    description: 'A Date() type inGraphQL as a scalar',
    serialize(value) {
        return value.toISOString();
    },
});
const resolvers = {
    Query: {
        about:() => aboutMessage,
        issueList,
    },
    Mutation: {
        setAboutMessage,
    },
    GraphQLDate,
};

function setAboutMessage(_, {message}) {
    return aboutMessage = message;
}
function issueList(){
    return issuesDB;
}
const server = new ApolloServer({
    typeDefs: fs.readFileSync('./server/schema.graphql', 'utf-8'),
    resolvers,
});


const app = express();//Додаток Express - це веб-сервер, який прослуховує певну IP-адресу та порт. 
//Можна створити кілька додатків, які прослуховують різні порти, але ми цього робити не будемо, оскільки нам потрібен лише один сервер.

// const fileServerMiddleware = express.static('HalloWorld2019');
// app.use('/', fileServerMiddleware);
app.use(express.static('HalloWorld2019'));//попередні два рядки замінюємо на цей один
server.applyMiddleware({ app, path: '/graphql' });
app.listen(3000, function () {
    console.log('App started on port 3000');
});